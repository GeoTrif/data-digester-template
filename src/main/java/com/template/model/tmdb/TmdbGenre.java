package com.template.model.tmdb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties
public class TmdbGenre implements Serializable {

    private static final long serialVersionUID = -8140142513151157324L;

    private long id;
    private String name;

    public TmdbGenre() {
    }

    public TmdbGenre(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TmdbGenre{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

