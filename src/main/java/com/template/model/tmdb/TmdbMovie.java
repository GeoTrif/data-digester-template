package com.template.model.tmdb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "tmdbmovie")
public class TmdbMovie {

    @Id
    private long id;
    @Column(name = "homepage")
    private String homepage;
    @Column(name = "budget")
    private long budget;
    @Column(name = "adult")
    private boolean adult;
    @Column(name = "poster_path")
    private String posterPath;
    @Column(name = "release_date")
    private Date releaseDate;
    @Column(name = "runtime")
    private int runtime;
    @Column(name = "revenue")
    private long revenue;
    @Column(name = "title")
    private String title;
    @Column(name = "vote_average")
    private double voteAverage;
    @Column(name = "vote_count")
    private long voteCount;

    public TmdbMovie() {
    }

    public TmdbMovie(long id, String homepage, long budget, boolean adult, String posterPath, Date releaseDate, int runtime,
                     long revenue, String title, double voteAverage, long voteCount) {
        this.id = id;
        this.homepage = homepage;
        this.budget = budget;
        this.adult = adult;
        this.posterPath = posterPath;
        this.releaseDate = releaseDate;
        this.runtime = runtime;
        this.revenue = revenue;
        this.title = title;
        this.voteAverage = voteAverage;
        this.voteCount = voteCount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public long getBudget() {
        return budget;
    }

    public void setBudget(long budget) {
        this.budget = budget;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getRuntime() {
        return runtime;
    }

    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    public long getRevenue() {
        return revenue;
    }

    public void setRevenue(long revenue) {
        this.revenue = revenue;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public long getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(long voteCount) {
        this.voteCount = voteCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TmdbMovie tmdbMovie = (TmdbMovie) o;
        return id == tmdbMovie.id &&
                budget == tmdbMovie.budget &&
                adult == tmdbMovie.adult &&
                runtime == tmdbMovie.runtime &&
                revenue == tmdbMovie.revenue &&
                Double.compare(tmdbMovie.voteAverage, voteAverage) == 0 &&
                voteCount == tmdbMovie.voteCount &&
                Objects.equals(homepage, tmdbMovie.homepage) &&
                Objects.equals(posterPath, tmdbMovie.posterPath) &&
                Objects.equals(releaseDate, tmdbMovie.releaseDate) &&
                Objects.equals(title, tmdbMovie.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, homepage, budget, adult, posterPath, releaseDate, runtime, revenue, title, voteAverage, voteCount);
    }

    @Override
    public String toString() {
        return "TmdbMovie{" +
                "id=" + id +
                ", homepage='" + homepage + '\'' +
                ", budget=" + budget +
                ", adult=" + adult +
                ", posterPath='" + posterPath + '\'' +
                ", releaseDate=" + releaseDate +
                ", runtime=" + runtime +
                ", revenue=" + revenue +
                ", title='" + title + '\'' +
                ", voteAverage=" + voteAverage +
                ", voteCount=" + voteCount +
                '}';
    }
}
