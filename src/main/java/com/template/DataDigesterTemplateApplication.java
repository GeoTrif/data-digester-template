package com.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataDigesterTemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataDigesterTemplateApplication.class, args);
    }

/*
https://rapidapi.com/blog/movie-api/
https://developers.themoviedb.org/3/getting-started/introduction
https://docs.oracle.com/javase/7/docs/api/java/security/MessageDigest.html
https://www.ibm.com/support/knowledgecenter/SSB23S_1.1.0.2020/gtps7/msdgapi.html
https://blog.bigml.com/2012/11/12/digesting-big-data/
 */

}
