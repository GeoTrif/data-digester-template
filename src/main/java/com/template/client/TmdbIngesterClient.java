package com.template.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.template.model.tmdb.TmdbMovieRaw;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Component
public class TmdbIngesterClient {

    private static final Logger LOGGER = LogManager.getLogger(TmdbIngesterClient.class);

    private static final String EMPTY_STRING = "";
    private static final String GET_TMDB_RAW_MOVIE_MESSAGE = "Starting getting from TMDB the movie {}";
    private static final String COULD_NOT_DESERIALIZE_TMDB_PAYLOAD_ERROR_MESSAGE = "Couldn't deserialize TMDB payload: {}";
    private static final String COULD_NOT_GET_TMDB_RESPONSE_BODY_ERROR_MESSAGE = "Couldn't get TMDB response body for id: {}";

    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final RestTemplate REST_TEMPLATE = new RestTemplate();

    @Value("${tmdb.api.key}")
    private String tmdbApiKey;
    @Value("${tmdb.base.url}")
    private String tmdbBaseUrl;

    public TmdbMovieRaw getTmdbRawMovie(Long id) {
        LOGGER.info(GET_TMDB_RAW_MOVIE_MESSAGE, id);

        String movieString = getResponseBodyfromTmdbRequest(id);

        return transformResponseBodyToTmdbRawMovie(movieString);
    }

    private String getResponseBodyfromTmdbRequest(Long id) {
        String movieString = EMPTY_STRING;

        try {
            movieString = REST_TEMPLATE.getForObject(String.format(tmdbBaseUrl, id, tmdbApiKey), String.class);
        } catch (HttpClientErrorException e) {
            LOGGER.error(COULD_NOT_GET_TMDB_RESPONSE_BODY_ERROR_MESSAGE, id, e);
        }

        return movieString;
    }

    private TmdbMovieRaw transformResponseBodyToTmdbRawMovie(String movieString) {
        TmdbMovieRaw tmdbMovieRaw = new TmdbMovieRaw();

        try {
            tmdbMovieRaw = MAPPER.readValue(movieString, TmdbMovieRaw.class);
        } catch (JsonProcessingException e) {
            LOGGER.error(COULD_NOT_DESERIALIZE_TMDB_PAYLOAD_ERROR_MESSAGE, movieString, e);
        }

        return tmdbMovieRaw;
    }
}
