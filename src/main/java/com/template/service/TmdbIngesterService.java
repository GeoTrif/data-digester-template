package com.template.service;

import com.template.client.TmdbIngesterClient;
import com.template.model.tmdb.TmdbMovie;
import com.template.model.tmdb.TmdbMovieRaw;
import com.template.repository.TmdbMovieRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TmdbIngesterService {

    private static final Logger LOGGER = LogManager.getLogger(TmdbIngesterService.class);

    private static final String GET_TMDB_MOVIES_START_MESSAGE = "Starting TMDB data ingestion with startId: %d and endId: %d";
    private static final String SAVING_TMDB_MOVIE_ERROR_MESSAGE = "Couldn't save TMDB movie with id: %d to database.\n %s";
    private static final String SAVING_TMDB_MOVIE_MESSAGE = "Saving TMDB movie with id: %d to database.\n %s";

    private final TmdbIngesterClient dataIngesterClient;
    private final TmdbMovieRepository tmdbMovieRepository;

    public TmdbIngesterService(final TmdbIngesterClient dataIngesterClient, final TmdbMovieRepository tmdbMovieRepository) {
        this.dataIngesterClient = dataIngesterClient;
        this.tmdbMovieRepository = tmdbMovieRepository;
    }

    public List<TmdbMovieRaw> getTmdbMovies(long startId, long endId) {
        LOGGER.info(String.format(GET_TMDB_MOVIES_START_MESSAGE, startId, endId));

        List<TmdbMovieRaw> tmdbRawMovies = new ArrayList<>();

        for (long i = startId; i <= endId; i++) {
            TmdbMovieRaw rawMovie = dataIngesterClient.getTmdbRawMovie(i);
            tmdbRawMovies.add(rawMovie);
            TmdbMovie movie = transformTmdbMovieRawToTmdbMovie(rawMovie);

            if (movie.getId() != 0) {
                tmdbMovieRepository.save(movie);

                LOGGER.info(String.format(SAVING_TMDB_MOVIE_MESSAGE, movie.getId(), movie));
            } else {
                LOGGER.error(String.format(SAVING_TMDB_MOVIE_ERROR_MESSAGE, i, movie));
            }
        }

        return tmdbRawMovies;
    }

    private TmdbMovie transformTmdbMovieRawToTmdbMovie(TmdbMovieRaw tmdbMovieRaw) {
        TmdbMovie movie = new TmdbMovie();
        movie.setId(tmdbMovieRaw.getId());
        movie.setHomepage(tmdbMovieRaw.getHomepage());
        movie.setBudget(tmdbMovieRaw.getBudget());
        movie.setAdult(tmdbMovieRaw.isAdult());
        movie.setPosterPath(tmdbMovieRaw.getPosterPath());
        movie.setReleaseDate(tmdbMovieRaw.getReleaseDate());
        movie.setRuntime(tmdbMovieRaw.getRuntime());
        movie.setRevenue(tmdbMovieRaw.getRevenue());
        movie.setTitle(tmdbMovieRaw.getTitle());
        movie.setVoteAverage(tmdbMovieRaw.getVoteAverage());
        movie.setVoteCount(tmdbMovieRaw.getVoteCount());

        return movie;
    }
}
