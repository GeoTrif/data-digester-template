package com.template.repository;

import com.template.model.tmdb.TmdbMovie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TmdbMovieRepository extends JpaRepository<TmdbMovie, Long> {
}
