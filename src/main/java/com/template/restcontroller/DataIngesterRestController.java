package com.template.restcontroller;

import com.template.model.tmdb.TmdbMovieRaw;
import com.template.service.TmdbIngesterService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DataIngesterRestController {

    private static final String GET_TMDB_MOVIES_ENDPOINT = "/get-tmdb-movies";

    private final TmdbIngesterService dataIngesterService;

    public DataIngesterRestController(final TmdbIngesterService dataIngesterService) {
        this.dataIngesterService = dataIngesterService;
    }

    @GetMapping(GET_TMDB_MOVIES_ENDPOINT)
    public List<TmdbMovieRaw> getTmdbMovies(@RequestParam long startId, @RequestParam long endId) {
        return dataIngesterService.getTmdbMovies(startId, endId);
    }
}
