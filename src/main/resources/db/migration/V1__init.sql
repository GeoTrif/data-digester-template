CREATE TABLE IF NOT EXISTS tmdbmovie (
`id` BIGINT NOT NULL,
`homepage` VARCHAR(80),
`budget` BIGINT,
`adult` TINYINT,
`poster_path` VARCHAR(80),
`release_date` DATE,
`runtime` INT,
`revenue` BIGINT,
`title` VARCHAR(80),
`vote_average` DOUBLE,
`vote_count` BIGINT,
PRIMARY KEY (`id`)
) ENGINE=InnoDB;